From the beginning Dean-Smith Realty has been a leader in the sale of houses and land in the greater Knoxville area and surrounding counties. Founded on hometown knowledge, the work ethic of the family farmer and an insatiable desire to provide expert service with honesty and professional demeanor.

Address: 412 S Northshore Dr, Knoxville, TN 37919, USA

Phone: 865-588-5000
